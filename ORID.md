## ORID

### O:

1. This morning, I learned how to tasking. The tasking should be Independent, simple and have Input and output. After that, I learned context map, we draw that after tasking. Under the guidance of a teacher, we drew the first context map about finding the cute number. In the process, I have a clearer understanding of the implementation process of this requirement. 
2. we also study something about Git, I learned how to set alias to command. And we should submit a comment for the specification, we should categorize the submission, like feat, fix, refactor and so on. In the afternoon, we coding after drawing context map.  

### R:

1. Context map is a completely new concept for me, but it help me to have clearer understanding of the implementation process of the requirement. In addition, it makes coding smoother
2. Alias command bring great convenience to us.
3. During the process of drawing context map, I awared that my java basic is still weak.

### I:

1. Tasking is a way to make us understand the requirement better, there are big hidden dangers if we coding without that.
2. Context map let us have a clearer understanding of the Implementation process of requirements.

### D:

In future development, I will not coding directly, but tasking the requirement first. If I have any doubt with the requirement, I will ask for the BA. And I can also set some alias for my git tools. And I should spend extra time to learn more java basic.

